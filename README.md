## Swedbank test task

#### Run dev profile with H2 database
````./gradlew bootRun --args='--spring.profiles.active=dev'````

#### RESTful endpoints
_Add consumption record_
* ```POST /api/consumption```

example of payload (list of objects)
````
[{ 
	"driverId": 10001,
	"fuelType": "95",
	"date": "21.09.2019",
	"price": "2.20",
	"volume": "20.02"
}]
````

_Get all fuel consumption records_
* ```GET /api/consumption```

_Get all fuel consumption records for specific driver_
* ```GET /api/consumption/{driverId}```

Get fuel consumption records by month
* ```GET /api/consumption?month=```

Get fuel consumption records by month and for specific driver
* ```GET /api/consumption/{drvierId}?month=```

Get total sum grouped by month
* ```GET /api/consumption/total```

Get total sum grouped by month and for specific driver
* ```GET /api/consumption/{driverId}/total```

Get statistics
* ```GET /api/consumption/statistics```

Get statistics by driver
* ```GET /api/consumption/{driverId}/statistics```
