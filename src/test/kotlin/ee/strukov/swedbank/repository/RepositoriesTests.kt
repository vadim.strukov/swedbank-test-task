package ee.strukov.swedbank.repository

import ee.strukov.swedbank.model.FuelConsumption
import ee.strukov.swedbank.model.FuelType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.text.SimpleDateFormat
import java.util.*

@ExtendWith(SpringExtension::class)
@DataJpaTest
class RepositoriesTests @Autowired constructor(
        val fuelConsumptionRepository: FuelConsumptionRepository
) {
    private val driverId: Long = 10000

    @Test
    fun `When findAllByDriverId then return List of FuelConsumption with requested driver id`() {
        saveTestData()
        val found = fuelConsumptionRepository.findAllByDriverId(driverId)
        assertThat(found.size).isEqualTo(2)
        assertThat(found[0].driverId).isEqualTo(driverId)
        assertThat(found[1].driverId).isEqualTo(driverId)
    }

    @Test
    fun `When findTotalSpent then return list of StatisticsProjection`() {
        saveTestData()
        val found = fuelConsumptionRepository.findTotalSpent(driverId)
        assertThat(found.size).isEqualTo(2)
        assertThat(found[0].totalSpent).isEqualTo("60.94".toBigDecimal())
        assertThat(found[1].totalSpent).isEqualTo("21.04".toBigDecimal())
        assertThat(found[0].month).isEqualTo("07-2019")
        assertThat(found[1].month).isEqualTo("08-2019")
    }


    @Test
    fun `When gatherStatistics then  AmountProjection per month with calculated totalSpent`() {
        saveTestData()
        val found = fuelConsumptionRepository.gatherStatistics(driverId)
        assertThat(found.size).isEqualTo(2)
        assertThat(found[0].totalSpent).isEqualTo("60.94".toBigDecimal())
        assertThat(found[0].month).isEqualTo("07-2019")
        assertThat(found[0].totalVolume).isEqualTo("11.00".toBigDecimal())
        assertThat(found[0].averagePrice).isEqualTo("5.54".toBigDecimal())
        assertThat(found[0].fuel).isEqualTo(FuelType.FUEL_D)

        assertThat(found[1].totalSpent).isEqualTo("21.04".toBigDecimal())
        assertThat(found[1].month).isEqualTo("08-2019")
        assertThat(found[1].totalVolume).isEqualTo("13.40".toBigDecimal())
        assertThat(found[1].averagePrice).isEqualTo("1.57".toBigDecimal())
        assertThat(found[1].fuel).isEqualTo(FuelType.FUEL_D)
    }


    @Test
    fun `When findAllByMonth then return List of FuelConsumption with requested month only`() {
        saveTestData()
        val found = fuelConsumptionRepository.findAllByMonth(8)
        assertThat(found.size).isEqualTo(2)
        val date = Calendar.getInstance()
        date.time = found[0].date
        assertThat(date.get(Calendar.MONTH)).isEqualTo(Calendar.AUGUST)
        date.time = found[1].date
        assertThat(date.get(Calendar.MONTH)).isEqualTo(Calendar.AUGUST)
    }

    //Dirty solution because of @BeforeAll doesn't support non-static functions
    private fun saveTestData() {
        val fuelConsumption = FuelConsumption(
                driverId = driverId,
                fuelType = FuelType.FUEL_D,
                date = SimpleDateFormat("dd.MM.yyyy").parse("21.07.2019"),
                volume = "11.00".toBigDecimal(),
                price = "5.54".toBigDecimal()
        )
        val fuelConsumption2 = FuelConsumption(
                driverId = driverId,
                fuelType = FuelType.FUEL_D,
                date = SimpleDateFormat("dd.MM.yyyy").parse("21.08.2019"),
                volume = "13.40".toBigDecimal(),
                price = "1.57".toBigDecimal()
        )
        val fuelConsumption3 = FuelConsumption(
                driverId = 11111,
                fuelType = FuelType.FUEL_D,
                date = SimpleDateFormat("dd.MM.yyyy").parse("21.08.2019"),
                volume = "1.00".toBigDecimal(),
                price = "1.00".toBigDecimal()
        )
        fuelConsumptionRepository.saveAll(listOf(fuelConsumption, fuelConsumption2, fuelConsumption3))
    }
}