package ee.strukov.swedbank.repository

import ee.strukov.swedbank.model.FuelConsumption
import ee.strukov.swedbank.model.FuelType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.math.BigDecimal

@Repository
interface FuelConsumptionRepository : JpaRepository<FuelConsumption, Long> {

    fun findAllByDriverId(driverId: Long): List<FuelConsumption>

    @Query(value = "FROM FuelConsumption WHERE MONTH(date) = :month")
    fun findAllByMonth(month: Int): List<FuelConsumption>

    @Query(value = "FROM FuelConsumption WHERE MONTH(date) = :month AND driverId = :driverId")
    fun findAllByMonth(month: Int, driverId: Long): List<FuelConsumption>

    @Query(value = "SELECT ROUND(SUM(price * volume), 2) as totalSpent, to_char(date, 'MM-yyyy') as month " +
            "FROM Fuel_Consumption " +
            "GROUP BY month", nativeQuery = true)
    fun findTotalSpent(): List<AmountProjection>

    @Query(value = "SELECT ROUND(SUM(price * volume), 2) as totalSpent, to_char(date, 'MM-yyyy') as month  " +
            "FROM Fuel_Consumption WHERE driver_Id = ?1 " +
            "GROUP BY month", nativeQuery = true)
    fun findTotalSpent(driverId: Long): List<AmountProjection>

    @Query(value = "SELECT SUM(volume) as totalVolume, ROUND(SUM(price * volume), 2) as totalSpent, " +
            "ROUND(AVG(price), 2) as averagePrice, fuel_Type as fuel, to_char(date, 'MM-yyyy') as month " +
            "FROM Fuel_Consumption " +
            "GROUP BY fuel, month", nativeQuery = true)
    fun gatherStatistics(): List<StatisticsProjection>

    @Query(value = "SELECT SUM(volume) as totalVolume, ROUND(SUM(price * volume), 2) as totalSpent, " +
            "ROUND(AVG(price), 2) as averagePrice, fuel_Type as fuel, to_char(date, 'MM-yyyy') as month " +
            "FROM Fuel_Consumption WHERE driver_Id = ?1 " +
            "GROUP BY fuel, month", nativeQuery = true)
    fun gatherStatistics(driverId: Long): List<StatisticsProjection>
}


interface AmountProjection {
    val totalSpent: BigDecimal
    val month: String
}

interface StatisticsProjection {
    val fuel: FuelType
    val totalVolume: BigDecimal
    val totalSpent: BigDecimal
    val averagePrice: BigDecimal
    val month: String
}