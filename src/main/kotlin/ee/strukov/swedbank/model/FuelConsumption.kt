package ee.strukov.swedbank.model

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import ee.strukov.swedbank.round
import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.util.*
import javax.persistence.*
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

@Entity
data class FuelConsumption(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,
        @get: Min(value = 10000, message = "Driver ID should be bigger than 10000")
        @get: NotNull(message = "Driver id is mandatory")
        val driverId: Long,
        @get: NotNull(message = "Fuel type is mandatory")
        @Enumerated(value = EnumType.STRING)
        val fuelType: FuelType,
        @get: NotNull(message = "Date is mandatory")
        @get: DateTimeFormat(pattern = "dd.MM.yyyy")
        @get: JsonFormat(pattern="dd.MM.yyyy")
        val date: Date,
        @get: NotNull(message = "Price is mandatory")
        @get: DecimalMin(value = "0.00", message = "Price cannot be less than 0")
        val price: BigDecimal,
        @get: NotNull(message = "Volume is mandatory")
        @get: DecimalMin(value = "1.00", message = "Volume cannot be less than 1.00L")
        val volume: BigDecimal,

        @Transient
        var totalPrice: BigDecimal? = null
) {
        @PostLoad
        private fun onLoad() {
                totalPrice = (price * volume).round()
        }
}

enum class FuelType {
        @JsonProperty("95") FUEL_95,
        @JsonProperty("98") FUEL_98,
        @JsonProperty("D") FUEL_D
}