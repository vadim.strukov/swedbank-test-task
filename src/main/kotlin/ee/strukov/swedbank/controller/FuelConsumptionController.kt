package ee.strukov.swedbank.controller

import ee.strukov.swedbank.model.FuelConsumption
import ee.strukov.swedbank.repository.AmountProjection
import ee.strukov.swedbank.repository.StatisticsProjection
import ee.strukov.swedbank.round
import ee.strukov.swedbank.service.FuelConsumptionService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class FuelConsumptionController(private val fuelConsumptionService: FuelConsumptionService) {

    // Get all fuel consumption records
    @GetMapping("/consumption")
    fun getAll(): ResponseEntity<List<FuelConsumption>> = ResponseEntity
            .ok(fuelConsumptionService.findAll())

    // Get all fuel consumption records for specific driver
    @GetMapping("/consumption/{driverId}")
    fun getAllByDriverId(@PathVariable driverId: Long): ResponseEntity<List<FuelConsumption>> = ResponseEntity
            .ok(fuelConsumptionService.findAllByDriverId(driverId))

    // Get fuel consumption records by month
    @GetMapping("/consumption", params = ["month"])
    fun getAllByMonth(@RequestParam month: Int): ResponseEntity<List<FuelConsumption>> = ResponseEntity
            .ok(fuelConsumptionService.findAllByMonth(month))

    // Get fuel consumption records by month and for specific driver
    @GetMapping("/consumption/{driverId}", params = ["month"])
    fun getAllByMonthAndId(@RequestParam month: Int, @PathVariable driverId: Long): ResponseEntity<List<FuelConsumption>> = ResponseEntity
            .ok(fuelConsumptionService.findAllByMonth(month, driverId))

    //Get total sum grouped by month
    @GetMapping("/consumption/total")
    fun getTotalSpent(): ResponseEntity<List<AmountProjection>> = ResponseEntity
            .ok(fuelConsumptionService.findTotalSpent())

    //Get total sum grouped by month and for specific driver
    @GetMapping("/consumption/{driverId}/total")
    fun getTotalSpent(@PathVariable driverId: Long): ResponseEntity<List<AmountProjection>> = ResponseEntity
            .ok(fuelConsumptionService.findTotalSpent(driverId))

    //Get statistics
    @GetMapping("/consumption/statistics")
    fun getStatistics(): ResponseEntity<List<StatisticsProjection>> = ResponseEntity
            .ok(fuelConsumptionService.gatherStatistics())

    //Get statistics by driver
    @GetMapping("/consumption/{driverId}/statistics")
    fun getStatistics(@PathVariable driverId: Long): ResponseEntity<List<StatisticsProjection>> = ResponseEntity
            .ok(fuelConsumptionService.gatherStatistics(driverId))

    //Add consumption record
    @PostMapping("/consumption")
    fun save(@Valid @RequestBody fuelConsumption: List<FuelConsumption>): ResponseEntity<List<FuelConsumption>> = ResponseEntity
            .ok(fuelConsumptionService.save(fuelConsumption))



}