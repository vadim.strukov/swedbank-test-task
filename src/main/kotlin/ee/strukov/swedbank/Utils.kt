package ee.strukov.swedbank

import java.math.BigDecimal
import java.math.RoundingMode

fun BigDecimal.round(): BigDecimal {
   return this.setScale(2, RoundingMode.HALF_UP)
}