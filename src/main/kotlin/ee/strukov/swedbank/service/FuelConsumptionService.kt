package ee.strukov.swedbank.service

import ee.strukov.swedbank.model.FuelConsumption
import ee.strukov.swedbank.repository.AmountProjection
import ee.strukov.swedbank.repository.FuelConsumptionRepository
import ee.strukov.swedbank.repository.StatisticsProjection
import ee.strukov.swedbank.round
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class FuelConsumptionService(private val fuelConsumptionRepository: FuelConsumptionRepository) {

    @Transactional(readOnly = true)
    fun findAll(): List<FuelConsumption> = fuelConsumptionRepository
            .findAll()

    @Transactional(readOnly = true)
    fun findAllByDriverId(driverId: Long): List<FuelConsumption> = fuelConsumptionRepository
            .findAllByDriverId(driverId)

    @Transactional
    fun save(fuelConsumption: List<FuelConsumption>): List<FuelConsumption> {
        return fuelConsumptionRepository
                .saveAll(fuelConsumption.map { item -> item.copy(price = item.price.round()) })
    }

    @Transactional(readOnly = true)
    fun findAllByMonth(month: Int): List<FuelConsumption> = fuelConsumptionRepository
            .findAllByMonth(month)

    @Transactional(readOnly = true)
    fun findAllByMonth(month: Int, driverId: Long): List<FuelConsumption> = fuelConsumptionRepository
            .findAllByMonth(month, driverId)

    @Transactional(readOnly = true)
    fun findTotalSpent(): List<AmountProjection> = fuelConsumptionRepository
            .findTotalSpent()

    @Transactional(readOnly = true)
    fun findTotalSpent(driverId: Long): List<AmountProjection> = fuelConsumptionRepository
            .findTotalSpent(driverId)

    @Transactional(readOnly = true)
    fun gatherStatistics(): List<StatisticsProjection> = fuelConsumptionRepository
            .gatherStatistics()

    @Transactional(readOnly = true)
    fun gatherStatistics(driverId: Long): List<StatisticsProjection> = fuelConsumptionRepository
            .gatherStatistics(driverId)
}